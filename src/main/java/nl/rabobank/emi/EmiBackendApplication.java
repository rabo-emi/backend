package nl.rabobank.emi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmiBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmiBackendApplication.class, args);
	}

}

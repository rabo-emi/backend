package nl.rabobank.emi.model;

public record LoanRequestDao(
        Double loanValue,
        Double interestRate,
        Double loanTermMonths) { }

package nl.rabobank.emi.model;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

public class LoanRequestDto {

    @NotNull(message = "Loan value is required")
    @Positive(message = "Loan value must be greater then 0")
    private final Double loanValue;
    @NotNull(message = "Interest rate is required")
    @Positive(message = "Interest rate must be greater then 0")
    @DecimalMax(value = "100", message = "Interest rate must be not greater then 100")
    private final Double interestRate;
    @NotNull(message = "Loan term is required")
    @Positive(message = "Loan term must be greater then 0")
    @DecimalMax(value = "30", message = "Loan term must be not greater then 100")
    private final Double loanTerm;

    public LoanRequestDto(Double loanValue, Double interestRate, Double loanTerm) {
        this.loanValue = loanValue;
        this.interestRate = interestRate;
        this.loanTerm = loanTerm;
    }

    public LoanRequestDao toLoanRequestDao() {
        return new LoanRequestDao(
                this.loanValue,
                // TODO add const and comments
                this.interestRate / 100,
                this.loanTerm * 12);
    }
}

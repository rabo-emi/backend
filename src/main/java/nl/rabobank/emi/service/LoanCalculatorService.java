package nl.rabobank.emi.service;

import nl.rabobank.emi.model.LoanRequestDao;

public interface LoanCalculatorService {

    Double calculateEMI(LoanRequestDao request);
}

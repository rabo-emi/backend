package nl.rabobank.emi.service.impl;

import nl.rabobank.emi.model.LoanRequestDao;
import nl.rabobank.emi.service.LoanCalculatorService;
import org.springframework.stereotype.Service;

@Service
public class LoanCalculatorServiceImpl implements LoanCalculatorService {

    @Override
    public Double calculateEMI(LoanRequestDao request) {
        Double value = request.loanValue();
        Double rate = request.interestRate();
        Double term = request.loanTermMonths();

        return value * rate
                * Math.pow((1 + rate), term)
                / (Math.pow((1 + rate), term) - 1);
    }

}

package nl.rabobank.emi.controller;

import jakarta.validation.Valid;
import nl.rabobank.emi.model.LoanRequestDto;
import nl.rabobank.emi.service.LoanCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LoanCalculatorController {

    @Autowired
    private LoanCalculatorService loanCalculatorService;

    @GetMapping(path="/calculate", produces="application/json")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @CrossOrigin(origins = "http://localhost:4200") // Temporary property for local dev, should be properly handled if deployed
    private @ResponseBody ResponseEntity<Double> calculate(@Valid LoanRequestDto requestDto) {
        Double calculatedEmi = loanCalculatorService.calculateEMI(requestDto.toLoanRequestDao());
        return ResponseEntity.ok(calculatedEmi);
    }
}

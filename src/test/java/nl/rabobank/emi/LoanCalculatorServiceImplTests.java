package nl.rabobank.emi;

import nl.rabobank.emi.model.LoanRequestDao;
import nl.rabobank.emi.service.LoanCalculatorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LoanCalculatorServiceImplTests {

	@Autowired
	private LoanCalculatorService loanCalculatorService;
	@Test
	void shouldReturnCorrectEmiValueWhenRequestIsValid() {

		LoanRequestDao request = new LoanRequestDao(2d, 0.1, 12d);

		Double expected = 0.29352663020057446;
		Double actual = loanCalculatorService.calculateEMI(request);

		assertEquals(actual, expected);
	}

}

package nl.rabobank.emi;

import nl.rabobank.emi.model.LoanRequestDao;
import nl.rabobank.emi.service.impl.LoanCalculatorServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class LoanCalculatorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LoanCalculatorServiceImpl service;

    @Test
    public void shouldReturnOk() throws Exception {
        LoanRequestDao request = new LoanRequestDao(2d, 0.1, 12d);
        when(service.calculateEMI(request)).thenReturn(0.29352663020057446);

        MultiValueMap<String, String> parameters =new LinkedMultiValueMap<>();
        parameters.add("loanValue", "10");
        parameters.add("interestRate", "10");
        parameters.add("loanTerm", "1");

        mockMvc.perform(get("/calculate")
                .params(parameters))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnBadRequestWhenParameterIsMissing() throws Exception {
        MultiValueMap<String, String> parameters =new LinkedMultiValueMap<>();
        parameters.add("interestRate", "10");
        parameters.add("loanTerm", "1");

        mockMvc.perform(get("/calculate")
                        .params(parameters))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Loan value is required"));
    }

    @Test
    public void shouldReturnBadRequestWhenParameterIsBellowZero() throws Exception {
        MultiValueMap<String, String> parameters =new LinkedMultiValueMap<>();
        parameters.add("loanValue", "10");
        parameters.add("interestRate", "10");
        parameters.add("loanTerm", "-1");

        mockMvc.perform(get("/calculate")
                        .params(parameters))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Loan term must be greater then 0"));
    }

    @Test
    public void shouldReturnBadRequestWhenParameterIsAboveSetMax() throws Exception {
        MultiValueMap<String, String> parameters =new LinkedMultiValueMap<>();
        parameters.add("loanValue", "10");
        parameters.add("interestRate", "200");
        parameters.add("loanTerm", "1");

        mockMvc.perform(get("/calculate")
                        .params(parameters))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Interest rate must be not greater then 100"));
    }
}
